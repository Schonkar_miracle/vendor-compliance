import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, Router, NavigationStart, NavigationEnd, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { VendorModule } from './contents/vendor/vendor.module';

import { DataService } from './services/data.service';

import { AppComponent } from './app.component';
import { TopbarComponent } from './topbar/topbar.component';
import { FooterComponent } from './footer/footer.component';
import { LandingComponent } from './contents/landing/landing.component';
import { SearchComponent } from './contents/search/search.component';
import { StepsTrackerComponent } from './steps-tracker/steps-tracker.component';

export const routes: Routes = [
  { path: '', component: LandingComponent },
  { path: 'home', component: LandingComponent },
  { path: 'search', component: SearchComponent }

];

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    FooterComponent,
    LandingComponent,
    SearchComponent,
    StepsTrackerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    VendorModule,
    ChartsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
