import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit {
  data = [];
  constructor(private dataService: DataService) {
    this.data = dataService.getData();
  }

  ngOnInit() {
  }

}
