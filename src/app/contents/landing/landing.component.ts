import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.less']
})
export class LandingComponent implements OnInit {
  data = [];
  docs = [];
  advancedSearch = false;

  constructor(private dataService: DataService) {
    this.data = dataService.getData();
    this.docs = dataService.getDocuments();
  }

  donutChart = {
    "labels" : ['Compliant Agents', 'Partially Compliant Agents', 'Non Compliant Agents'],
    "data" : [50, 100, 260],
    "colors" : [
      {
        backgroundColor: ["#A1CF6B", "#ffaf66", "rgba(232, 116, 97, 0.8)"],
        // backgroundColor: ["#A1CF6B", "#ffaf66", "#E87461"],
        // backgroundColor: ["#240772", "#F87060", "#84BCDA"],
        // backgroundColor: ["#240772", "#a884ff", "#d7c6ff"],
      }
    ],
    "options" : {
      responsive: true,
      legend: {
        display: false
      }
    }
  };

  barChart = {
    "labels" : ['document1', 'document2', 'document3', 'document4', 'document5', 'document6', 'document7', 'document8'],
    "data" : [50, 40, 30, 20, 19, 86, 92, 92],
    "colors" : [
      {
        backgroundColor: "#a884ff",
      }
    ],
    "options" : {
      backgroundColor: "#ccc",
      scales: {
        xAxes: [{
          barPercentage: 0.4
        }]
      },
      responsive: true,
      legend: {
        display: false
      }
    }
  };

  onChartClick(event) {
    console.log(event);
  }

  ngOnInit() {
  }

}
