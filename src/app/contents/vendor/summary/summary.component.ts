import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.less']
})
export class SummaryComponent implements OnInit {
  currentJustify = 'justified';
  vendorID = 0;
  agent; docs; data;
  iterators;
  constructor(private route: ActivatedRoute, private dataService: DataService) {
    this.docs = dataService.getDocuments();
    this.route.params.subscribe(params => {
      this.vendorID = params.agent;
      this.agent = dataService.getData(this.vendorID);
    });
    this.iterators = Array(23).fill(1).map((x,i)=>i);
  }


  ngOnInit() {
  }

  toggleClass($event){
    $event.currentTarget.classList.toggle("text-disabled");
  }

}
