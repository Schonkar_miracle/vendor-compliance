import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../services/data.service';

@Component({
  selector: 'app-tab-documents',
  templateUrl: './tab-documents.component.html',
  styleUrls: ['../tabs-common.less', './tab-documents.component.less']
})
export class TabDocumentsComponent implements OnInit {
  documents=[];

  constructor(private dataService: DataService) {
    this.documents = dataService.getDocuments();
   }

  ngOnInit() {
  }

}
